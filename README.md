# nkbcgeneral

Generella verktyg för bearbetning av data från Nationellt
kvalitetsregister för bröstcancer (NKBC).

Detta R-paket är en **central** plats för definition, implementering och
dokumentation av **generell bearbetning** av NKBC-data i **alla**
utdata-kanaler.

-   Användning på INCA-plattformen tillsammans med R-paketet
    [nkbcind](https://bitbucket.org/cancercentrum/nkbcind)
    -   Onlinerapporter innanför inloggning på INCA-plattformen med
        R-paketet
        [rccShiny](https://bitbucket.org/cancercentrum/rccshiny)
    -   NKBC Koll på läget (KPL), med R-paketet
        [rccKPL](https://bitbucket.org/cancercentrum/rcckpl)
    -   NKBC Vården i siffror, med R-paketet
        [incavis](https://bitbucket.org/cancercentrum/incavis)
-   Användning lokalt på RCC Stockholm-Gotland
    -   Framtagande av NKBC Interaktiva Årsrapport med R-paketen
        [nkbcind](https://bitbucket.org/cancercentrum/nkbcind) och
        [rccShiny](https://bitbucket.org/cancercentrum/rccshiny)
    -   Datauttagsärenden inom NKBC
        -   jfr
            <https://www.cancercentrum.se/samverkan/vara-uppdrag/kunskapsstyrning/kvalitetsregister/datauttag/>
    -   Andra sammanställningar

Jfr
<https://www.cancercentrum.se/samverkan/vara-uppdrag/statistik/kvalitetsregisterstatistik/>

## Installation

``` r
if (!requireNamespace("remotes")) {
  install.packages("remotes")
}

remotes::install_bitbucket("cancercentrum/nkbcgeneral")
```

## Användning

Läs in ögonblicksbild av NKBC exporterad från INCA.

``` r
load(
  file.path(Sys.getenv("BRCA_DATA_DIR"), "2022-09-02", "nkbc_nat_avid 2022-09-02 07-08-17.RData")
)
```

Generell förbearbetning av NKBC-data.

``` r
df_nkbc_nat_avid_w_d_vars <- df |>
  dplyr::mutate(dplyr::across(tidyselect::where(is.factor), as.character)) |>
  dplyr::rename_with(
    # Döp om INCA:s variabelsuffix "_Värde" till "_Varde" eftersom
    # R-paket inte kan byggas om variabelnamn innehåller specialtecken
    stringr::str_replace, tidyselect::ends_with("_Värde"),
    pattern = "_Värde", replacement = "_Varde"
  ) |>
  nkbcgeneral::clean_nkbc_data() |>
  nkbcgeneral::mutate_nkbc_derived_vars()
```

Titta på beräknade data för fall diagnosticerade 2008 - 2021, för exakt
beräkning se källkoden för `nkbcgeneral::mutate_nkbc_derived_vars()`,
t.ex. på
<https://bitbucket.org/cancercentrum/nkbcgeneral/src/master/R/mutate_nkbc_derived_vars.R>

``` r
df_nkbc_nat_avid_w_d_vars |>
  dplyr::filter(lubridate::year(a_diag_dat) %in% 2008:2021) |>
  dplyr::mutate(dplyr::across(tidyselect::ends_with("_Varde"), as.factor)) |>
  dplyr::select(
    # Primär behandling (d_prim_beh_Varde)
    # - 1: Primär operation
    # - 2: Preoperativ onkologisk behandling eller konservativ behandling
    # - 3: Ej operation eller fjärrmetastas/-er vid diagnos
    d_prim_beh_Varde,

    # Invasivitet (d_invasiv_Varde)
    # - 1: Invasiv cancer
    # - 2: Enbart cancer in situ
    d_invasiv_Varde,

    # ER-status (d_er_Varde)
    # - 1: Positiv
    # - 2: Negativ
    d_er_Varde,

    # PgR-status (d_pr_Varde)
    # - 1: Positiv
    # - 2: Negativ
    d_pr_Varde,

    # HER2-status (d_her2_Varde)
    # - 1: Positiv
    # - 2: Negativ
    d_her2_Varde,

    # Biologisk subtyp (d_trigrp_Varde)
    # -  1: Luminal
    # -  2: HER2-positiv
    # -  3: Trippelnegativ bröstcancer (TNBC)
    d_trigrp_Varde
  ) |>
  summary()
#>  d_prim_beh_Varde d_invasiv_Varde d_er_Varde   d_pr_Varde   d_her2_Varde
#>  1   :101128      1   :106728     1   :88465   1   :73934   1   :13404  
#>  2   : 13371      2   : 12191     2   :14341   2   :28725   2   :86250  
#>  3   :  6359      NA's:  1981     NA's:18094   NA's:18241   NA's:21246  
#>  NA's:    42                                                            
#>  d_trigrp_Varde
#>  1   :76948    
#>  2   :13325    
#>  3   : 9096    
#>  NA's:21531
```

Titta närmare på biologisk subtyp (trigrupp) för invasiva fall
diagnosticerade 2008 - 2021.

``` r
df_nkbc_nat_avid_w_d_vars |>
  dplyr::filter(
    lubridate::year(a_diag_dat) %in% 2008:2021,

    # Enbart fall med invasiv cancer
    d_invasiv_Varde == 1
  ) |>
  dplyr::mutate(dplyr::across(tidyselect::ends_with("_Varde"), as.factor)) |>
  dplyr::select(
    d_invasiv_Varde,
    d_trigrp_Varde
  ) |>
  summary()
#>  d_invasiv_Varde d_trigrp_Varde
#>  1:106728        1   :76948    
#>                  2   :13325    
#>                  3   : 9096    
#>                  NA's: 7359
```

Jfr med <https://statistik.incanet.se/brostcancer/> \> Population \>
Biologisk subtyp
